﻿using BinaryKits.Zpl.Label.Elements;
using BinaryKits.Zpl.Label;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;

namespace imgtozpl;

internal class Program
{
    const double MM_CONST = 25.4;

    const double PAPER_WIDTH_IN = 4;
    const double PAPER_HEIGHT_IN = 6;
    const double PAPER_DPMM = 8;

    static async Task Main(string[] args)
    {
        Console.Write("Image path-> ");
        string imagePath = Console.ReadLine()!.Replace("\"", "");
        byte[] imageBuff = await File.ReadAllBytesAsync(imagePath);
        using Bitmap image = new Bitmap(
            new MemoryStream(imageBuff));

        Console.WriteLine("Scaling image");
        Size scaledSize = new Size(
            (int)Math.Round(PAPER_WIDTH_IN * MM_CONST * PAPER_DPMM),
            (int)Math.Round(PAPER_HEIGHT_IN * MM_CONST * PAPER_DPMM));
        using Bitmap scaledImage = new Bitmap(
            image,
            scaledSize);
        using MemoryStream scaledImageStream = new MemoryStream();
        scaledImage.Save(scaledImageStream, ImageFormat.Jpeg);

        Console.WriteLine("Generating zpl");
        var renderEngine = new ZplEngine(new ZplElementBase[]
        {
            new ZplDownloadGraphics('R', "IMAGE", scaledImageStream.ToArray()),
            new ZplRecallGraphic(0, 0, 'R', "IMAGE")
        });
        var zplOutput = renderEngine.ToZplString(
            new ZplRenderOptions());

        Console.WriteLine("Saving output and zpl");
        await File.WriteAllBytesAsync(
            "output.jpg",
            scaledImageStream.ToArray());
        await File.WriteAllTextAsync(
            "output.txt",
            zplOutput);

        Console.WriteLine(
            "Saved to: {0}",
            Path.Combine(
                Directory.GetCurrentDirectory(),
                "output.txt"));
    }
}
